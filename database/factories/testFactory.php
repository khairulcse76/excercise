<?php
use Carbon\Carbon;
$factory->define(App\test::class, function (Faker\Generator $faker) {

    return [
        'name'       => $faker->word,
        'password'       => $faker->password(),
        'created_at' => Carbon::now()->toDateTimeString(),
        'updated_at' => Carbon::now()->toDateTimeString(),
    ];

});