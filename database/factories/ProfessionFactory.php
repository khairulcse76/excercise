<?php
use Carbon\Carbon;

$factory->define(App\Profession::class, function (Faker\Generator $faker) {

    return [
        'name'       => $faker->word,
        'created_at' => Carbon::now()->toDateTimeString(),
        'updated_at' => Carbon::now()->toDateTimeString(),
    ];

});